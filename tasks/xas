Format: https://blends.debian.org/blends/1.1
Task: xas
Description: EXAFS and XANES
 This metapackage will install all X-ray photons-and-neutrons PAN EXAFS and XANES absorption spectroscopy packages.
Comment: Improve the description

Recommends: ifeffit

Recommends: horae

Depends: pymca

Depends: quantum-espresso

Recommends: python3-xraylarch

Suggests: xraylarch
Homepage: http://xraypy.github.io/xraylarch/
Vcs-Browser: https://salsa.debian.org/science-team/xraylarch
Vcs-Git: https://salsa.debian.org/science-team/xraylarch.git
License: BSD-3
Pkg-Description: X-ray Analysis for Synchrotron Applications using Python
 Larch is an open-source library and analysis toolkit for processing
 scientific data. Initially designed for x-ray spectroscopic and
 scattering data collected at modern synchrotrons and x-ray sources,
 Larch also provides many general-purpose processing and analysis
 tools for dealing with arrays of scientific data and organize complex
 data sets.
 .
 Larch is written in Python, making heavy use of the wonderful numpy,
 scipy, h5py, and matplotlib libraries, and can be used directly as a
 Python library or extended using Python. In addition, Larch provides
 a Python-like language (a macro language, or domain specific
 language) that is intended to be very easy to use for novices while
 also being complete enough for advanced data processing and analysis.
 .
 Larch has several related target application areas, including:
 .
  * XAFS analysis, becoming version 2 of the Ifeffit Package.
  * Visualizing and analyzing micro-X-ray fluorescence maps.
  * Quantitative X-ray fluorescence analysis.
  * X-ray standing waves and surface scattering analysis.
  * Data collection software for synchrotron data.
 .
 The essential idea is that having these different areas connected by
 a common macro language will strengthen the analysis tools available
 for all of them, and provide a very shallow barrier for those
 interested in scripting the manipulation and analysis of their data.
 .
 Currently, Larch provides a complete set of XAFS Analysis tools
 (replacing all of the Ifeffit package), has some support for
 visualizing and analyzing XRF maps and spectra, and has many extra
 tools for X-ray spectral analysis, data handling, and general-purpose
 data modeling.

Suggests: prestopronto
Homepage: http://code.google.com/p/prestopronto
License: GPL3+
Pkg-Description: Software for analysis of Quick Exafs data

Suggest: rixs
Homepage: internal SEXTANTS's beamline repository in Synchrotron SOLEIL
License: ?
Pkg-Description: Sorting and visualization of 1D spectra: XAS, scan positions, etc. (Detailed specifications sent to S. Bac in April 2020)
 Visualization of contextual data (energy and polarization of the line, resolution, sample and spectrometer positions)
 Processing of CCD images to extract RIXS spectra: application of thresholds, extraction of the curvature (by Gaussian fit) necessary to sum the  
 CCD lines after linear interpolation.
 Visualization of the RIXS spectra and construction of RIXS maps in two representations: Emission Energy and Energy Loss.
