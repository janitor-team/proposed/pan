Format: https://blends.debian.org/blends/1.1
Task: powder
Description: powder diffraction
 This metapackage will install all X-ray photons-and-neutrons PAN powder diffraction softwares.
Comment: Improve the description


Recommends: dioptas

Recommends: fityk

Recommends: libxy-bin

Recommends: objcryst-fox

Recommends: pyfai

Recommends: python3-pyxrd

Recommends: spd

Suggests: fullprof
Homepage: http://www.ill.eu/sites/fullprof
License: ?
Comment: binary only
Pkg-Description: ?

Suggests: jpowder
Homepage: http://www.jpowder.org/
License: GPL3+
Pkg-Description: Lightweight powder diffraction data visualizer

Suggests: xrdua
Homepage: http://sourceforge.net/projects/xrdua/
License: GPL3+
Pkg-Description: X-ray Powder Diffraction
Comment: test if it works with gdl

Suggests: pygsas
Homepage: https://subversion.xor.aps.anl.gov/trac/pyGSAS
License: DSFG
 Copyright 2010, 2020, UChicago Argonne, LLC, Operator of Argonne National
 Laboratory All rights reserved.
 .
 GSAS-II may be used by anyone on a royalty-free basis. Use and
 redistribution, with or without modification, are permitted provided
 that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Software changes, modifications, or derivative works should be noted
   with comments and the author and organization's name.
 * Distribution of changed, modified or derivative works based on
   GSAS-II grants the GSAS-II copyright holder unrestricted permission
   to include any, or all, new and changed code in future GSAS-II
   releases.
 * Redistributions that include binary forms must include all relevant
   source code and reproduce the above copyright notice, this list of
   conditions and the following disclaimers in the documentation and/or
   other materials provided with the distribution.
 * Neither the names of UChicago Argonne, LLC or the Department of
   Energy nor the names of its contributors may be used to endorse or
   promote products derived from this software without specific prior
   written permission.
 * The software and the end-user documentation included with the
   redistribution, if any, must include the following acknowledgment:
   "This product includes software produced by UChicago Argonne, LLC
   under Contract No. DE-AC02-06CH11357 with the Department of Energy."
Pkg-Description: crystallography multi purpose python library
 GSAS-II is an open source Python project that addresses all types of
 crystallographic studies, from simple materials through
 macromolecules, using both powder and single-crystal diffraction and
 with both x-ray and neutron probes. Measurements can be constant
 wavelength (or in the future, neutron TOF.) At present, code is being
 developed for all the various steps in diffraction analysis, such as
 data reduction, peak analysis, indexing, structure solution and
 structure refinement.
 .
 At present GSAS-II can be used for processing of area detector data,
 peak fitting, auto-indexing, structure solution, Pawley and Rietveld
 fitting. Many of the nice features of GSAS are present and some
 things work much better than in GSAS, but other features are yet to
 come. At this point only x-rays and constant wavelength neutrons are
 implemented (no TOF). The GUI is self-documenting with help pages for
 every GUI page. The code is changing on a regular basis, so expect
 things to break from time to time and you should 'Update' frequently
 to stay abrest of new features as they are added.

Suggests: pyxrd
Homepage: http://users.ugent.be/~madumon/pyxrd/index.html
License: BSD-2
Pkg-Description: matrix algorithm for X-ray diffraction patterns
 PyXRD is a python implementation of the matrix algorithm for computer
 modeling of X-ray diffraction (XRD) patterns of disordered lamellar
 structures. It's goals are to:
 .
   * provide an easy user-interface for end-users
   * provide basic tools for displaying and manipulating XRD patterns
   * produce high-quality (publication-grade) figures
   * make modelling of XRD patterns for mixed-layer clay minerals 'easy'
   * be free and open-source (open box instead of closed box model)
 .
 PyXRD was written with the multi-specimen full-profile fitting method
 in mind. A direct result is the ability to 'share' parameters among
 similar phases. This allows for instance to have an air-dry and a
 glycolated illite-smectite share their coherent scattering domain
 size, but still have different basal spacings and interlayer
 compositions for the smectite component.
 .
 Other features are (incomplete list):
 .
   * Import/export several common XRD formats (.RD, .RAW, .CPI, ASCII)
   * simple background subtraction/addition (linear or custom patterns)
   * smoothing patterns and adding noise to patterns
   * peak finding and annotating (markers)
   * custom line colors, line widths, pattern positions, ...
   * goniometer settings (wavelengths, geometry settings, ...)
   * specimen settings (sample length, absorption, ...)
   * automatic parameter refinement using several algorithms, e.g.:
      *  L BFGS B
      *  Brute Force
      *  Covariation Matrix Adapation Evolutionary Strategy (CMA-ES; using DEAP)
      *  Multiple Particle Swarm Optimization (MPSO; using DEAP)
   * scripting support
